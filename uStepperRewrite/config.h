/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__
#include "common.h"


/*
*****************************************************************************************
* OPTIONS
*****************************************************************************************
*/
// board type
#define __BOARD_PRO_MINI__          1
#define __BOARD_PRO_MICRO__         2



/*
*****************************************************************************************
* CONFIGURATION
*****************************************************************************************
*/
#define __FEATURE_BOARD__           __BOARD_PRO_MICRO__
#define __FEATURE_DEBUG__           1
#define __STD_SERIAL__              1


/*
*****************************************************************************************
* RULE CHECK
*****************************************************************************************
*/

/*
*****************************************************************************************
* PINS
*****************************************************************************************
*/
// MOTOR
#define PIN_MOT_ENABLE          7

#define PIN_MOT1_STEP           4
#define PIN_MOT1_DIR            5

#define PIN_MOT2_STEP           18
#define PIN_MOT2_DIR            19

#define PIN_MOT3_STEP           20
#define PIN_MOT3_DIR            21

// ENDSTOP
#define PIN_MOT1_ENDSTOP        14      // PCINT3
#define PIN_MOT2_ENDSTOP        15      // PCINT1
#define PIN_MOT3_ENDSTOP        16      // PCINT2

// SERVO
#define PIN_SERVO_PWM           6

// DROP IN
#define PIN_DROPIN_EN           8
#define PIN_DROPIN_STEP         9
#define PIN_DROPIN_DIR          10

// OTHERS
#define PIN_LED                 17

#define SERIAL_BPS              1000000 //115200


/*
*****************************************************************************************
* COMMON SETTINGS
*****************************************************************************************
*/
#define T1_DIV                  64
#define T1_FREQ                 (F_CPU / T1_DIV)

#define MOTOR_MICRO_STEPPING    8
#define MOTOR_STEPS_PER_ROT     200
#define MOTOR_FSPR              (MOTOR_STEPS_PER_ROT * MOTOR_MICRO_STEPPING)

#define DOF                     3
#define SHOULDER_TO_ELBOW       220.0f
#define ELBOW_TO_WRIST          220.0f
#define WRIST_TO_FINGER         0.0f

#define HOME_X                  220.0f
#define HOME_Y                  0.0f
#define HOME_Z                  220.0f

#define GEAR_RATIO_X            4.80 //4.18
#define GEAR_RATIO_Y            2.14
#define GEAR_RATIO_Z            4.80 //4.18

#define MM_PER_SEGMENT          3


/*
x gear - 11:45  = 4.09
y gear - 21:45  = 2.14
z gear - 11:45  = 4.09
*/


#endif
