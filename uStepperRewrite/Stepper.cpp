
#include <Arduino.h>
#include <stdarg.h>
#include <Wire.h>
#include <EEPROM.h>
#include "Stepper.h"


/*
*****************************************************************************************
* MACROS
*****************************************************************************************
*/
#define ALPHA                   (2 * 3.14159 / MOTOR_FSPR)          // 2*pi/spr
#define A_T_x100                ((u32)(ALPHA * T1_FREQ * 100))      // (ALPHA * T1_FREQ)*100
#define T1_FREQ_148             ((u32)((T1_FREQ * 0.676) / 100))    // divided by 100 and scaled by 0.676
#define A_SQ                    (u32)(ALPHA * 2 * 10000000000)      // ALPHA*2*10000000000
#define A_x20000                (u32)(ALPHA * 20000)                // ALPHA*20000

static Stepper *mParent;

static const u8 TBL_MOT_DIR_PIN[] PROGMEM = {
    PIN_MOT2_DIR, PIN_MOT1_DIR, PIN_MOT3_DIR
};

static const u8 TBL_MOT_STEP_PIN[] PROGMEM = {
    PIN_MOT2_STEP, PIN_MOT1_STEP, PIN_MOT3_STEP
};


/*
*****************************************************************************************
* mspCallback
*****************************************************************************************
*/
s8 mspCallback(void *param, u8 addr, u8 *data, u8 size, u8 *res)
{
    s8 ret = 0;
    Stepper *parent = (Stepper*)param;

    if (addr == SerialProtocol::ADDR_CONF) {
        DUMP("CONF_RX", data, size);
    } else if (addr == parent->getAddr()) {
        LOG(F("ADDR : %02x\n"), addr);
        DUMP("DATA", data, size);

        if (addr != SerialProtocol::ADDR_HOST) {
            u16 ctr = *((u16*)data);
            u16 *pRes = (u16*)res;

            *pRes = 0xffff - ctr;
            ret = sizeof(ctr);
        }
    }

    return ret;
}

void Stepper::doISR(u8 idx)
{
    u16 new_step_delay = 0;                 // Holds next delay period.

    nextISR(idx, mSRD[idx].step_delay);

    switch (mSRD[idx].run_state) {
        case Stepper::STOP:
            mSRD[idx].step_count = 0;
            mSRD[idx].rest = 0;
            enableTimer(idx, FALSE);
            break;

        case Stepper::ACCEL:
            step(idx, mSRD[idx].dir);
            mSRD[idx].step_count++;
            mSRD[idx].accel_count++;
            new_step_delay = mSRD[idx].step_delay - (((2 * (s32)mSRD[idx].step_delay) + mSRD[idx].rest) / (4 * mSRD[idx].accel_count + 1));
            mSRD[idx].rest = ((2 * (long)mSRD[idx].step_delay) + mSRD[idx].rest) % (4 * mSRD[idx].accel_count + 1);
            if (mSRD[idx].step_count >= mSRD[idx].decel_start) {                   // Chech if we should start decelration.
                mSRD[idx].accel_count = mSRD[idx].decel_val;
                mSRD[idx].run_state = Stepper::DECEL;
            } else if(new_step_delay <= mSRD[idx].min_delay) {           // Chech if we hitted max speed.
                mSRD[idx].last_accel_delay = new_step_delay;
                new_step_delay = mSRD[idx].min_delay;
                mSRD[idx].rest = 0;
                mSRD[idx].run_state = Stepper::RUN;
            }
            break;

        case Stepper::RUN:
            step(idx, mSRD[idx].dir);
            mSRD[idx].step_count++;
            new_step_delay = mSRD[idx].min_delay;
            if (mSRD[idx].step_count >= mSRD[idx].decel_start) {                   // Chech if we should start decelration.
                mSRD[idx].accel_count = mSRD[idx].decel_val;
                new_step_delay = mSRD[idx].last_accel_delay;                          // Start decelration with same delay as accel ended with.
                mSRD[idx].run_state = Stepper::DECEL;
            }
            break;

        case Stepper::DECEL:
            step(idx, mSRD[idx].dir);
            mSRD[idx].step_count++;
            mSRD[idx].accel_count++;
            new_step_delay = mSRD[idx].step_delay - (((2 * (s32)mSRD[idx].step_delay) + mSRD[idx].rest)/(4 * mSRD[idx].accel_count + 1));
            mSRD[idx].rest = ((2 * (s32)mSRD[idx].step_delay) + mSRD[idx].rest) % (4 * mSRD[idx].accel_count + 1);
            // Check if we at last step
            if (mSRD[idx].accel_count >= 0) {
                mSRD[idx].run_state = Stepper::STOP;
            }
            break;
    }
    mSRD[idx].step_delay = new_step_delay;
}

ISR(TIMER3_COMPA_vect)
{
    mParent->doISR(0);
}

ISR(TIMER3_COMPB_vect)
{
    mParent->doISR(1);
}

ISR(TIMER3_COMPC_vect)
{
    mParent->doISR(2);
}

void Stepper::move(u8 idx, s16 step, u16 accel, u16 decel, u16 speed)
{
    u16     max_s_lim;      //! Number of steps before we hit max speed.
    u16     accel_lim;      //! Number of steps before we must start deceleration (if accel does not hit max speed).

#if __FEATURE_DEBUG__
    u32     min_delay_exp3;
    u32     t;
    u16     s;
    u16     r;
    char    szR[4];
#endif

    if (idx == 2) {
        step = -step;
    }

    // Set direction from sign on step value.
    if (step < 0) {
        mSRD[idx].dir = CCW;
        step = -step;
    } else {
        mSRD[idx].dir = CW;
    }

    // If moving only 1 step.
    if (step == 1) {
        mSRD[idx].accel_count = -1;      // Move one step...
        mSRD[idx].run_state = DECEL;     // ...in DECEL state.
        mSRD[idx].step_delay = 1000;     // Just a short delay so main() can act on 'running'.
        nextISR(idx, 10);
        enableTimer(idx, TRUE);
    } else if (step != 0) {
        // Refer to documentation for detailed information about these calculations.
        // Set max speed limit, by calc min_delay to use in timer.
        // min_delay = (alpha / tt)/ w
        mSRD[idx].min_delay = A_T_x100 / speed;

#if __FEATURE_DEBUG__
        min_delay_exp3   = (s32)mSRD[idx].min_delay * 1000;
        s = min_delay_exp3 / T1_FREQ;
        t = min_delay_exp3 - (s * T1_FREQ);
        r = (t * 1000) / T1_FREQ;

        if (r < 10) {
            szR[0] = '0';
            szR[1] = '0';
            itoa(r, &szR[2], 10);
        } else if (r < 100) {
            szR[0] = '0';
            itoa(r, &szR[1], 10);
        } else {
            itoa(r, szR, 10);
        }
        LOG("min_delay=%u T=%d.%s ms\n", mSRD[idx].min_delay, s, szR);
#endif

        // Set accelration by calc the first (c0) step delay .
        // step_delay = 1/tt * sqrtl(2*alpha/accel)
        // step_delay = ( tfreq*0.676/100 )*100 * sqrtl( (2*alpha*10000000000) / (accel*100) )/10000
        mSRD[idx].step_delay = (T1_FREQ_148 * sqrtl(A_SQ / accel)) / 100;

        // Find out after how many steps does the speed hit the max speed limit.
        // max_s_lim = speed^2 / (2*alpha*accel)
        max_s_lim = (s32)speed * speed/ (s32)(((s32)A_x20000 * accel) / 100);

        // If we hit max speed limit before 0,5 step it will round to 0.
        // But in practice we need to move atleast 1 step to get any speed at all.
        if (max_s_lim == 0) {
            max_s_lim = 1;
        }

        // Find out after how many steps we must start deceleration.
        // n1 = (n1+n2)decel / (accel + decel)
        accel_lim = ((s32)step * decel) / (accel + decel);
        // We must accelrate at least 1 step before we can start deceleration.
        if (accel_lim == 0){
            accel_lim = 1;
        }

        // Use the limit we hit first to calc decel.
        if (accel_lim <= max_s_lim) {
            mSRD[idx].decel_val = accel_lim - step;
        } else {
            mSRD[idx].decel_val = -((s32)max_s_lim*accel)/decel;
        }
        // We must decelrate at least 1 step to stop.
        if(mSRD[idx].decel_val == 0){
            mSRD[idx].decel_val = -1;
        }

        // Find step to start decleration.
        mSRD[idx].decel_start = step + mSRD[idx].decel_val;

        // If the maximum speed is so low that we dont need to go via accelration state.
        if (mSRD[idx].step_delay <= mSRD[idx].min_delay) {
            mSRD[idx].step_delay = mSRD[idx].min_delay;
            mSRD[idx].run_state = RUN;
        } else {
            mSRD[idx].run_state = ACCEL;
        }

        // Reset counter.
        mSRD[idx].accel_count = 0;

        nextISR(idx, 100);
        enableTimer(idx, TRUE);
    }
}

void Stepper::enableTimer(u8 idx, bool enable)
{
    u8  bit = OCIE3A + idx;

    if (enable) {
        TIMSK3 |= _BV(bit);
    } else {
        TIMSK3 &= ~_BV(bit);
    }
}

void Stepper::nextISR(u8 idx, u16 delay)
{
    u16  tbl[3] = { (u16)&OCR3A, (u16)&OCR3B, (u16)&OCR3C };
    volatile u16 *reg = (volatile u16*)tbl[idx];

    *reg = TCNT3 + delay;
}

void Stepper::initTimer(void)
{
    u8 div;

    TCCR3A = 0;
    TCCR3B = 0;

    switch (T1_DIV) {
        case 8:
            div = _BV(CS31);
            break;

        case 64:
            div = _BV(CS31) | _BV(CS30);
            break;

        case 256:
            div = _BV(CS32);
            break;

        case 1024:
        default:
            div = _BV(CS32) | _BV(CS30);
            break;
    }

    TCCR3B |= div;
    TCNT3   = 0;
}

Stepper::Stepper()
{
    mParent = this;
}

void Stepper::setup(void)
{
    setAddr(SerialProtocol::ADDR_CONF);
    EEPROM.get(0, mInfo);
    pinMode(PIN_MOT_ENABLE,   OUTPUT);
    pinMode(PIN_MOT1_STEP,    OUTPUT);
    pinMode(PIN_MOT1_DIR,     OUTPUT);
    pinMode(PIN_MOT2_STEP,    OUTPUT);
    pinMode(PIN_MOT2_DIR,     OUTPUT);
    pinMode(PIN_MOT3_STEP,    OUTPUT);
    pinMode(PIN_MOT3_DIR,     OUTPUT);
    pinMode(PIN_MOT1_ENDSTOP, INPUT);
    pinMode(PIN_MOT2_ENDSTOP, INPUT);
    pinMode(PIN_MOT3_ENDSTOP, INPUT);
    pinMode(PIN_SERVO_PWM,    OUTPUT);
    pinMode(PIN_DROPIN_EN,    INPUT);
    pinMode(PIN_DROPIN_STEP,  INPUT);
    pinMode(PIN_DROPIN_DIR,   INPUT);
    enable(FALSE);

    mSerial.begin(SERIAL_BPS);
    mSerial.registerCallback(this, mspCallback);
    LOG(F("Let's start... ADDR: %02x\n"), getAddr());

    initTimer();
}

Stepper::~Stepper()
{

}

void Stepper::step(u8 idx, u8 dir)
{
    u8   pin;

    pin  = pgm_read_byte(&TBL_MOT_DIR_PIN[idx]);
    if (dir == CW) {
        digitalWrite(pin, LOW);
    } else {
        digitalWrite(pin, HIGH);
    }

    pin  = pgm_read_byte(&TBL_MOT_STEP_PIN[idx]);
    digitalWrite(pin, HIGH);
    __asm__ __volatile__ (
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop" "\n\t"
    "nop");
    digitalWrite(pin, LOW);
}

void Stepper::enable(bool en)
{
    if (en) {
        digitalWrite(PIN_MOT_ENABLE, LOW);
    } else {
        digitalWrite(PIN_MOT_ENABLE, HIGH);
    }
}

u32 Stepper::sqrtl(u32 x)
{
    register u32 xr;            // result register
    register u32 q2;            // scan-bit register
    register u8 f;              // flag (one bit)

    xr = 0;                     // clear result
    q2 = 0x40000000L;           // higest possible result bit

    do
    {
        if ((xr + q2) <= x) {
            x -= xr + q2;
            f = 1;              // set flag
        } else {
            f = 0;              // clear flag
        }
        xr >>= 1;
        if (f) {
            xr += q2;           // test flag
        }
    } while(q2 >>= 2);          // shift twice

    if (xr < x) {
        return xr +1;           // add for rounding
    }
    return xr;
}

void Stepper::save()
{
    EEPROM.put(0, mInfo);
}

void Stepper::loop(u32 ts)
{
    mSerial.handleMSP();

    if (getAddr() == SerialProtocol::ADDR_HOST) {
        if (ts - mTS > 500) {
            mSerial.sendMSPPacket(SerialProtocol::PT_CMD, 1, (u8*)&mCtr, sizeof(mCtr));
            mCtr++;
            mTS = ts;
        }
    }
}

