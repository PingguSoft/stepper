#include "RobotArm.h"
#include "Stepper.h"

#define MIN_STEP_DELAY       (1)
#define MAX_FEEDRATE         (1000000 / MIN_STEP_DELAY)
#define MIN_FEEDRATE         (0.01)
#define DEFAULT_FEEDRATE     (200)

static RobotArm *mParent;

void RobotArm::pause(u32 ms)
{
    delay(ms / 1000);
    delayMicroseconds(ms % 1000);  // delayMicroseconds doesn't work for values > ~16k.
}

RobotArm::RobotArm(float fShoulder2Elbow, float fElbow2Wrist, float fWrist2Finger)
{
    mConf.fShoulder2Elbow = fShoulder2Elbow;
    mConf.fElbow2Wrist    = fElbow2Wrist;
    mConf.fWrist2Finger   = fWrist2Finger;
    mConf.fBase2ShoulderX = 0;
    mConf.fBase2ShoulderZ = 0;

    mLast.fX = 0;
    mLast.fY = 0;
    mLast.fZ = 0;
    mLast.b  = -1;

    mCmdBuf.off = 0;

    mVar.bABS = MODE_ABS;
    mVar.idxTool = 0;
    for (u8 i = 0; i < MAX_TOOLS; i++) {
        mVar.vTools[i].set(0, 0, 0);
    }
    mLastTS = millis();
    feedrate(DEFAULT_FEEDRATE);

    mParent = this;
}

void RobotArm::setShoulderOffset(float fBase2ShoulderX, float fBase2ShoulderZ)
{
    mConf.fBase2ShoulderX = fBase2ShoulderX;
    mConf.fBase2ShoulderZ = fBase2ShoulderZ;
}

void RobotArm::dumpV(char *name, Vector3 v)
{
    mSerial->print(name);
    mSerial->print(F(" ("));
    mSerial->print(v.x);
    mSerial->print(F(", "));
    mSerial->print(v.y);
    mSerial->print(F(", "));
    mSerial->print(v.z);
    mSerial->println(F(")"));

}

bool RobotArm::doIK(float x, float y, float z, s32 &stepX, s32 &stepY, s32 &stepZ)
{
    float a, b, c;

    // if we know the position of the wrist relative to the shoulder
    // we can use intersection of circles to find the elbow.
    // once we know the elbow position we can find the angle of each joint.
    // each angle can be converted to motor steps.

    // use intersection of circles to find two possible elbow points.
    // the two circles are the bicep (shoulder-elbow) and the forearm (elbow-wrist)
    // the distance between circle centers is d
    Vector3 planeArm(x, y, 0);
    planeArm.normalize();

    // the finger (attachment point for the tool) is a short distance in "front" of the wrist joint
    Vector3 wrist(x, y, z);
    wrist -= (planeArm * mConf.fWrist2Finger);

    // shoulder
    Vector3 shoulder = planeArm;
    shoulder  *= (mConf.fBase2ShoulderX);
    shoulder.z = mConf.fBase2ShoulderZ;

    Vector3 es = wrist - shoulder;
    float    d = es.length();
    if (d == 0) {
        return FALSE;
    }

    float r1 = mConf.fElbow2Wrist;        // circle 1 centers on wrist
    float r0 = mConf.fShoulder2Elbow;     // circle 0 centers on shoulder
    if (d > mConf.fElbow2Wrist + mConf.fShoulder2Elbow) {
        // The points are impossibly far apart, no solution can be found.
        return FALSE;
    }

    a = (r0 * r0 - r1 * r1 + d * d) / (2.0 * d);
    // find the midpoint
    Vector3 mid = es * ( a / d ) + shoulder;
    // with a and r0 we can find h, the distance from midpoint to the intersections.
    float h = sqrt(r0 * r0 - a * a);
    // the distance h on a line orthogonal to n and plane_normal gives us the two intersections.
    Vector3 n(-planeArm.y, planeArm.x, 0);
    Vector3 r = es ^ n;
    r.normalize();
    Vector3 elbow = mid + r * h;

    // find the shoulder angle using atan3(elbow-shoulder)
    Vector3 temp = elbow - shoulder;
    float   ax   = temp | planeArm;
    float   ay   = elbow.z;
    a = atan3(ay, ax);

    // find the elbow angle
    temp = elbow - wrist;
    float bx = temp | planeArm;
    float by = temp.z;
    c = atan3(by, bx);

#if 0
    dumpV("elbow", elbow);
    dumpV("wrist", wrist);
    dumpV("temp", temp);
    mSerial->print(by);
    mSerial->print(", ");
    mSerial->println(bx);
#endif

    // the easiest part
    b = atan3(y, x);

//    if (mLast.b >= 0) {
        if (abs(b - mLast.b) > PI) {
            LOG(F("JUMP\n"));
            if (mLast.b < b) {
                mLast.stepY = (mLast.b + (2.0 * PI)) * MOTOR_FSPR * GEAR_RATIO_Y / (2 * PI);
            } else {
                mLast.stepY = (mLast.b - (2.0 * PI)) * MOTOR_FSPR * GEAR_RATIO_Y / (2 * PI);
            }
        }
//    }

    mSerial->print(a);
    mSerial->print(F(" rad -> "));
    mSerial->print(RAD2DEG(a));
    mSerial->println(F(" deg"));


    stepX = -MOTOR_FSPR * GEAR_RATIO_X * a / (2 * PI);
    stepY =  MOTOR_FSPR * GEAR_RATIO_Y * b / (2 * PI);
    stepZ =  MOTOR_FSPR * GEAR_RATIO_Z * c / (2 * PI);

    LOG(F("   - (%6d, %6d, %6d) angle (%6d, %6d, %6d) steps (%6ld, %6ld, %6ld) diff(%6ld, %6ld, %6ld)\n"), (int)x, (int)y, (int)z,
        (int)RAD2DEG(a), (int)RAD2DEG(b), (int)RAD2DEG(c),
        stepX, stepY, stepZ,
        stepX - mLast.stepX, stepY - mLast.stepY, stepZ - mLast.stepZ);

    mLast.b = b;

    return TRUE;
}

void RobotArm::pos(float x, float y, float z)
{
    mLast.fX = x;
    mLast.fY = y;
    mLast.fZ = z;
}

void RobotArm::setHome(float x, float y, float z)
{
    LOG(F("HOME (%6d, %6d, %6d)\n"), (int)x, (int)y, (int)z);
    pos(x, y, z);
    doIK(x, y, z, mLast.stepX, mLast.stepY, mLast.stepZ);
}

void RobotArm::lineToStep(s32 stepX, s32 stepY, s32 stepZ)
{
    mAxis[0].delta = (stepX - mLast.stepX);
    mAxis[1].delta = (stepY - mLast.stepY);
    mAxis[2].delta = (stepZ - mLast.stepZ);

    u8   i, pins;
    u32  j, maxSteps = 0;

    for (i = 0; i < DOF; i++) {
        mAxis[i].absdelta = abs(mAxis[i].delta);
        mAxis[i].dir      = mAxis[i].delta > 0 ? CW : CCW;
        if (maxSteps < mAxis[i].absdelta)
            maxSteps = mAxis[i].absdelta;
        mAxis[i].over = 0;
    }

    for (j = 0; j < maxSteps; j++) {
        for (i = 0; i < DOF; i++) {
            pins = PINB;
            mAxis[i].over += mAxis[i].absdelta;
            if (mAxis[i].over >= maxSteps) {
                mAxis[i].over -= maxSteps;
//                if (mAxis[i].dir == CCW && isSet(pins, digitalPinToBitMask(PIN_MOT1_ENDSTOP + i))) {
//                    LOG(F("ENDSTOP%d detected !\n"), i + 1);
//                } else {
                    Stepper::step(i, mAxis[i].dir);
//                }
            }
        }
        pause(mVar.delayStep);
    }

    mLast.stepX = stepX;
    mLast.stepY = stepY;
    mLast.stepZ = stepZ;
}

void RobotArm::lineToSub(float x, float y, float z)
{
    s32 stepX, stepY, stepZ;

    if (doIK(x, y, z, stepX, stepY, stepZ)) {
        lineToStep(stepX, stepY, stepZ);
    }
    pos(x, y, z);
}

void RobotArm::lineTo(float x, float y, float z)
{
    x -= mVar.vTools[mVar.idxTool].x;
    y -= mVar.vTools[mVar.idxTool].y;
    z -= mVar.vTools[mVar.idxTool].z;

    // split up long lines to make them straighter?
    Vector3 destination(x, y, z);
    Vector3 start(mLast.fX, mLast.fY, mLast.fZ);
    Vector3 dp = destination - start;
    Vector3 temp;

    float len = dp.length();
    u16   pieces = floor(len / (float)MM_PER_SEGMENT );
    LOG(F("len:%d, pieces:%d\n"), (u16)len, pieces);

    if (pieces == 0)
        return;

    float a;
    for (u16 j = 1; j < pieces; j++) {
        a = (float)j / (float)pieces;
        temp = dp * a + start;
        lineToSub(temp.x, temp.y, temp.z);
    }
    lineToSub(x, y, z);

#if 0
    float dx = x - mLast.fX;
    float dy = y - mLast.fY;
    float dz = z - mLast.fZ;

    float len   = sqrt(dx * dx + dy * dy + dz * dz);
    u32   segments = floor(len / MM_PER_SEGMENT);

    float x0 = mLast.fX;
    float y0 = mLast.fY;
    float z0 = mLast.fZ;
    float inc;

    LOG(F("len:%d, seg:%ld\n"), (u16)len, segments);
    for (u32 i = 1; i < segments; i++) {
        inc = (float)i / (float)segments;
        lineToSub(dx * inc + x0, dy * inc + y0, dz * inc + z0);
    }
    lineToSub(x, y, z);
#endif
}

// returns angle of dy / dx as a value from 0...2PI
float RobotArm::atan3(float dy, float dx)
{
    float a = atan2(dy, dx);
    if (a < 0)
        a = (PI * 2.0) + a;

    return a;
}


void RobotArm::arc(float x, float y, float z, float cx, float cy, u8 dir)
{
    // get radius
    float dx = mLast.fX - cx;
    float dy = mLast.fY - cy;
    float radius = sqrt(dx * dx + dy * dy);

    // find angle of arc (sweep)
    float angle1 = atan3(dy, dx);
    float angle2 = atan3(y - cy, x - cx);
    float theta  = angle2 - angle1;

    if(dir > 0 && theta < 0)
        angle2 += 2 * PI;
    else if(dir < 0 && theta > 0)
        angle1 += 2 * PI;

    theta = angle2 - angle1;

    // get length of arc
    // float circ=PI*2.0*radius;
    // float len=theta*circ/(PI*2.0);
    // simplifies to
    float len = abs(theta) * radius;
    u32   segments = floor(len / (float)MM_PER_SEGMENT);
    float nx, ny, nz, angle3, scale;

    LOG(F("arc len:%d, seg:%ld\n"), (u16)len, segments);
    for (u32 i = 1; i < segments; ++i) {
        // interpolate around the arc
        scale  = ((float)i) / ((float)segments);
        angle3 = (theta * scale) + angle1;
        nx     = cx + cos(angle3) * radius;
        ny     = cy + sin(angle3) * radius;
        nz     = (z - mLast.fZ) * scale + mLast.fZ;

        // send it to the planner
        lineTo(nx, ny, nz);
    }
    lineTo(x, y, z);
}

void RobotArm::ready(void)
{
    mCmdBuf.off = 0;
    mSerial->print(F(">"));
}

void RobotArm::where(void)
{
    mSerial->print(F("X"));
    mSerial->println(mLast.fX);
    mSerial->print(F("Y"));
    mSerial->println(mLast.fY);
    mSerial->print(F("Z"));
    mSerial->println(mLast.fZ);
    mSerial->print(F("F"));
    mSerial->println(mVar.feedrate);
    mSerial->println(mVar.bABS ? F("ABS") : F("REL"));
}

float RobotArm::parseNumber(u8 code, float val)
{
    u8 *ptr = mCmdBuf.buf;

    while (ptr && *ptr && ptr < mCmdBuf.buf + mCmdBuf.off) {
        if (*ptr == code) {
            return atof(ptr + 1);
        }
        ptr = strchr(ptr, ' ');
        ptr = (ptr) ? (ptr + 1) : NULL;
    }
    return val;
}

void RobotArm::feedrate(float nfr)
{
    if(mVar.feedrate == nfr)
        return;  // same as last time?  quit now.

    if(nfr > MAX_FEEDRATE || nfr < MIN_FEEDRATE) {  // don't allow crazy feed rates
        mSerial->print(F("New feedrate must be greater than "));
        mSerial->print(MIN_FEEDRATE);
        mSerial->print(F("steps/s and less than "));
        mSerial->print(MAX_FEEDRATE);
        mSerial->println(F("steps/s."));
        return;
    }
    mVar.delayStep = 1000000.0 / nfr;
    mVar.feedrate = nfr;
}

void RobotArm::setToolOffset(u8 idx, float x, float y, float z)
{
    mVar.vTools[idx].x = x;
    mVar.vTools[idx].y = y;
    mVar.vTools[idx].z = z;
}

Vector3 RobotArm::getEndWithToolOffset(void)
{
    return Vector3(mVar.vTools[mVar.idxTool].x + mLast.fX,
                   mVar.vTools[mVar.idxTool].y + mLast.fY,
                   mVar.vTools[mVar.idxTool].z + mLast.fZ);
}

void RobotArm::changeTool(u8 idx)
{
    if (idx > MAX_TOOLS) {
        idx = MAX_TOOLS - 1;
    }
    mVar.idxTool = idx;
}

#define VERSION              (2)  // firmware version

void RobotArm::hello(void)
{
    mSerial->print(F("Arm3-v1 "));
    mSerial->println(VERSION);
    mSerial->println(F("Commands:"));
    mSerial->println(F("M18; - disable motors"));
    mSerial->println(F("M100; - this help message"));
    mSerial->println(F("M114; - report position and feedrate"));
    mSerial->println(F("F, G00, G01, G04, G17, G18, G28, G54-G59, G90-G92, M06 as described by http://en.wikipedia.org/wiki/G-code"));
}

void RobotArm::processGCode(void)
{
    u16     cmd;
    u16     idx;
    Vector3 offset;
    float   x, y, z, i, j;

    cmd = parseNumber('G', -1);
    switch (cmd) {
        // line
        case 0:
        case 1:
            feedrate(parseNumber('F', mVar.feedrate));
            offset = getEndWithToolOffset();
            x = parseNumber('X', (mVar.bABS ? offset.x : 0)) + (mVar.bABS ? 0 : offset.x);
            y = parseNumber('Y', (mVar.bABS ? offset.y : 0)) + (mVar.bABS ? 0 : offset.y);
            z = parseNumber('Z', (mVar.bABS ? offset.z : 0)) + (mVar.bABS ? 0 : offset.z);
            lineTo(x, y, z);
            break;

        // arc
        case 2:
        case 3:
            feedrate(parseNumber('F', mVar.feedrate));
            offset = getEndWithToolOffset();
            x = parseNumber('X', (mVar.bABS ? offset.x : 0)) + (mVar.bABS ? 0 : offset.x);
            y = parseNumber('Y', (mVar.bABS ? offset.y : 0)) + (mVar.bABS ? 0 : offset.y);
            z = parseNumber('Z', (mVar.bABS ? offset.z : 0)) + (mVar.bABS ? 0 : offset.z);
            i = parseNumber('I', (mVar.bABS ? offset.x : 0)) + (mVar.bABS ? 0 : offset.x);
            j = parseNumber('J', (mVar.bABS ? offset.y : 0)) + (mVar.bABS ? 0 : offset.y);
            arc(x, y, z, i, j, (cmd == 2));
            break;

        // pause
        case 4:
            pause(parseNumber('P', 0) * 1000);
            break;

        case 54:
        case 55:
        case 56:
        case 57:
        case 58:
        case 59:
            idx = cmd - 54;
            x = parseNumber('X', mVar.vTools[idx].x);
            y = parseNumber('Y', mVar.vTools[idx].y);
            z = parseNumber('Z', mVar.vTools[idx].z);
            setToolOffset(idx, x, y, z);
            break;

        case 90:
            mVar.bABS = MODE_ABS;
            break;

        case 91:
            mVar.bABS = MODE_REL;
            break;

        // set logical position
        case 92:
            offset = getEndWithToolOffset();
            x = parseNumber('X', offset.x);
            y = parseNumber('Y', offset.y);
            z = parseNumber('Z', offset.z);
            pos(x, y, z);
            break;

        default:
            break;
    }

    cmd = parseNumber('M', -1);
    switch (cmd) {
        case 6:
            changeTool(parseNumber('T', 0));
            break;

        case 17:
            Stepper::enable(TRUE);
            break;

        case 18:
            Stepper::enable(FALSE);
            break;

        case 100:
            break;

        case 114:
            where();
            break;

        case 1000:
        case 1001:
        case 1002:
            cmd = cmd - 1000;
            idx = parseNumber('D', 0) == 0 ? CW : CCW;
            for (int k = 0; k < 10; k++) {
                Stepper::step(cmd, idx);
            }
            break;

        default:
            break;
    }
}

void RobotArm::setup(Stream *serial)
{
    pinMode(PIN_MOT1_ENDSTOP, INPUT);
    pinMode(PIN_MOT2_ENDSTOP, INPUT);
    pinMode(PIN_MOT3_ENDSTOP, INPUT);
    digitalWrite(PIN_MOT1_ENDSTOP, HIGH);
    digitalWrite(PIN_MOT2_ENDSTOP, HIGH);
    digitalWrite(PIN_MOT3_ENDSTOP, HIGH);

/*
    volatile u8 *reg;

    // enable pin change interrupt
    reg = digitalPinToPCICR(PIN_MOT1_ENDSTOP);
    *reg |= _BV(digitalPinToPCICRbit(PIN_MOT1_ENDSTOP));

    // enable pin change mask
    reg = digitalPinToPCMSK(PIN_MOT1_ENDSTOP);
    *reg |= _BV(digitalPinToPCMSKbit(PIN_MOT1_ENDSTOP)) | _BV(digitalPinToPCMSKbit(PIN_MOT2_ENDSTOP)) | _BV(digitalPinToPCMSKbit(PIN_MOT3_ENDSTOP));
*/

//    PCICR  = _BV(PCIE0);
//    PCMSK0 = _BV(PCINT1) | _BV(PCINT2) | _BV(PCINT3);

    mSerial = serial;
    hello();
    setHome(HOME_X, HOME_Y, HOME_Z);
    ready();
}

void RobotArm::loop(u32 ts)
{
    u8  ch;

    if (mSerial->available() > 0) {
        ch = mSerial->read();
        mSerial->print((char)ch);

        if (mCmdBuf.off < MAX_BUF) {
            mCmdBuf.buf[mCmdBuf.off++] = ch;
            if (ch == '\r' || ch == '\n') {
                mCmdBuf.buf[mCmdBuf.off++] = 0;
                mSerial->print(F("\r\n"));
                processGCode();
                ready();
            }
        } else {
            processGCode();
            ready();
        }
    }
}

