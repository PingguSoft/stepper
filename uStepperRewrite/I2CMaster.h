#ifndef _I2C_MASTER_H_
#define _I2C_MASTER_H_
#include "common.h"
#include "config.h"
#include "utils.h"

class I2CMaster
{
    private:
        /** Contains the status of the I2C bus */
        u8 status;

        void (*mEnableInt)(bool enable);


        /**
         * @brief      Sends commands over the I2C bus.
         *
         *             This function is used to send different commands over the
         *             I2C bus.
         *
         * @param      cmd   - Command to be send over the I2C bus.
         */
        bool cmd(u8 cmd);

    public:
        void registerIntCtrlRoutine(void (*func)(bool enable)) { mEnableInt = func; }



        /**
         * @brief      Constructor
         *
         *             This is the constructor, used to instantiate an I2C
         *             object. Under normal circumstances, this should not be
         *             needed by the programmer of the arduino sketch, since
         *             this library already has a global object instantiation of
         *             this class, called "I2C".
         */
        I2CMaster(void);

        /**
         * @brief      Reads a byte from the I2C bus.
         *
         *             This function requests a byte from the device addressed
         *             during the I2C transaction setup. The parameter "ack" is
         *             used to determine whether the device should keep sending
         *             data or not after the reception of the currently
         *             requested data byte.
         *
         * @param      ack   - should be set to "ACK" if more bytes is wanted,
         *                   and "NACK" if no more bytes should be send (without
         *                   the quotes)
         * @param      data  - Address of the variable to store the requested
         *                   data byte
         *
         * @return     Always returns 1
         */
        bool readByte(bool ack, u8 *data);

        /**
         * @brief      sets up I2C connection to device, reads a number of data
         *             bytes and closes the connection
         *
         *             This function is used to perform a read transaction
         *             between the arduino and an I2C device. This function will
         *             perform everything from setting up the connection,
         *             reading the desired number of bytes and tear down the
         *             connection.
         *
         * @param      slaveAddr   -    7 bit address of the device to read from
         * @param      regAddr     -    8 bit address of the register to read from
         * @param      numOfBytes  -    Number of bytes to read from the device
         * @param      data        -    Address of the array/string to store the
         *                         bytes read. Make sure enough space are
         *                         allocated before calling this function !
         *
         * @return     1            -   Currently always returns this value. In the future
         *             this value will be used to indicate successful
         *             transactions.
         */
        bool read(u8 slaveAddr, u8 regAddr, u8 numOfBytes, u8 *data);

        /**
         * @brief      sets up connection between arduino and I2C device.
         *
         *             This function sets up the connection between the arduino
         *             and the I2C device desired to communicate with, by
         *             sending a start condition on the I2C bus, followed by the
         *             device address and a read/write bit.
         *
         * @param      addr  -  Address of the device it is desired to
         *                   communicate with
         * @param      RW    -  Can be set to "READ" to setup a read transaction
         *                   or "WRITE" for a write transaction (without the
         *                   quotes)
         *
         * @return     1        -   Connection properly set up
         * @return     0        -   Connection failed
         */
        bool start(u8 addr, bool RW);

        /**
         * @brief      Restarts connection between arduino and I2C device.
         *
         *             This function restarts the connection between the arduino
         *             and the I2C device desired to communicate with, by
         *             sending a start condition on the I2C bus, followed by the
         *             device address and a read/write bit.
         *
         * @param      addr  -  Address of the device it is desired to
         *                   communicate with
         * @param      RW    -  Can be set to "READ" to setup a read transaction
         *                   or "WRITE" for a write transaction (without the
         *                   quotes)
         *
         * @return     1        -   Connection properly set up
         * @return     0        -   Connection failed
         */
        bool restart(u8 addr, bool RW);

        /**
         * @brief      Writes a byte to a device on the I2C bus.
         *
         *             This function writes a byte to a device on the I2C bus.
         *
         * @param      data  - Byte to be written
         *
         * @return     1    - Byte written successfully
         * @return     0    - transaction failed
         */
        bool writeByte(u8 data);

        /**
         * @brief      sets up I2C connection to device, writes a number of data
         *             bytes and closes the connection
         *
         *             This function is used to perform a write transaction
         *             between the arduino and an I2C device. This function will
         *             perform everything from setting up the connection,
         *             writing the desired number of bytes and tear down the
         *             connection.
         *
         * @param      slaveAddr   -    7 bit address of the device to write to
         * @param      regAddr     -    8 bit address of the register to write to
         * @param      numOfBytes  -    Number of bytes to write to the device
         * @param      data        -    Address of the array/string containing data
         *                         to write.
         *
         * @return     1            -   Currently always returns this value. In the future
         *             this value will be used to indicate successful
         *             transactions.
         */
        bool write(u8 slaveAddr, u8 regAddr, u8 numOfBytes, u8 *data);

        /**
         * @brief      Closes the I2C connection
         *
         *             This function is used to close down the I2C connection,
         *             by sending a stop condition on the I2C bus.
         *
         * @return     1    -   This is always returned Currently - in a later
         *             version, this should indicate that connection is
         *             successfully closed
         */
        bool stop(void);

        /**
         * @brief      Get current I2C status
         *
         *             This function returns the status of the I2C bus.
         *
         * @return     Status of the I2C bus. Refer to defines for possible
         *             status
         */
        u8 getStatus(void);

        /**
         * @brief      Setup TWI (I2C) interface
         *
         *             This function sets up the TWI interface, and is
         *             automatically called in the instantiation of the uStepper
         *             encoder object.
         */
        void begin(u32 freq);
};
/** Global definition of I2C object for use in arduino sketch */

extern I2CMaster I2C;


#endif
