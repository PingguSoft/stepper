/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef _ROBOT_ARM_H_
#define _ROBOT_ARM_H_
#include <Arduino.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "vector3.h"


// all unit is mm (milli meter)
#define PI              3.14159
#define RAD2DEG(rad)    (rad * 180.0 / PI)
#define DEG2RAD(deg)    (deg * PI / 180.0)
#define MAX_TOOLS       6
#define MODE_REL        0
#define MODE_ABS        1
#define MAX_BUF         64

class RobotArm
{
    public:
        RobotArm(float fShoulder2Elbow, float fElbow2Wrist, float fWrist2Finger);
        void setShoulderOffset(float fBase2ShoulderX, float fBase2ShoulderZ);
        void setHome(float x, float y, float z);
        void lineTo(float x, float y, float z);
        void arc(float x, float y, float z, float cx, float cy, u8 dir);
        void loop(u32 ts);
        void setup(Stream *serial);

    private:
        void  pos(float x, float y, float z);
        void  lineToStep(s32 stepX, s32 stepY, s32 stepZ);
        void  lineToSub(float x, float y, float z);
        float atan3(float dy, float dx);
        bool  doIK(float x, float y, float z, s32 &stepX, s32 &stepY, s32 &stepZ);
        void  pause(u32 ms);
        void  feedrate(float nfr);

        void  ready(void);
        void  where(void);
        float parseNumber(u8 code, float val);
        void  setToolOffset(u8 idx, float x, float y, float z);
        Vector3 getEndWithToolOffset(void);
        void  changeTool(u8 idx);
        void  processGCode(void);
        void  hello(void);
        void  dumpV(char *name, Vector3 v);


        struct {
            float fBase2ShoulderX;
            float fBase2ShoulderZ;

            float fShoulder2Elbow;
            float fElbow2Wrist;
            float fWrist2Finger;
        } mConf;

        struct {
            float fX;
            float fY;
            float fZ;

            s32 stepX;
            s32 stepY;
            s32 stepZ;

            float b;
        } mLast;

        struct {
            s32 delta;
            u32 absdelta;
            u8  dir;
            s32 over;
        } mAxis[DOF];


        struct {
            u8      bABS;
            float   feedrate;
            u32     delayStep;
            Vector3 vTools[MAX_TOOLS];
            u8      idxTool;;
        } mVar;

        struct {
            u8  off;
            u8  buf[MAX_BUF];
        } mCmdBuf;

        Stream *mSerial;
        u32     mLastTS;
};

#endif

