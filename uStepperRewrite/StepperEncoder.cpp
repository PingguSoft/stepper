#include <Arduino.h>
#include "StepperEncoder.h"


/** Frequency at which the encoder is sampled, for keeping track of angle moved and current speed */
#define ENCODERINTFREQ          1000.0
/** Constant to convert angle difference between two interrupts to speed in revolutions per second */
#define ENCODERSPEEDCONSTANT    ENCODERINTFREQ/10.0/4096.0
/** I2C address of the encoder chip */
#define ENCODERADDR             0x36
/** Address of the register, in the encoder chip, containing the 8 least significant bits of the stepper shaft angle */
#define ANGLE                   0x0E
/** Address of the register, in the encoder chip, containing information about whether a magnet has been detected or not */
#define STATUS                  0x0B
/** Address of the register, in the encoder chip, containing information about the current gain value used in the encoder chip. This value should preferably be around 127 (Ideal case!) */
#define AGC                     0x1A
/** Address of the register, in the encoder chip, containing the 8 least significant bits of magnetic field strength measured by the encoder chip */
#define MAGNITUDE               0x1B

static StepperEncoder *parent;

#define MAKE_WORD(a) (((u16)(a[0]) << 8) | (u16)(a[1]))


ISR(TIMER1_COMPA_vect)
{
    parent->nextISR();
}

inline void enableInt(bool enable)
{
    if (enable) {
        TIMSK1 |= _BV(OCIE1A);
    } else {
        TIMSK1 &= ~_BV(OCIE1A);
    }
}

StepperEncoder::StepperEncoder(void)
{
    mI2C.begin(400000);
    mI2C.registerIntCtrlRoutine(enableInt);
    parent = this;
}

float StepperEncoder::getAngleMoved(void)
{
    return (float)mAngleMoved * 0.087890625;
}

float StepperEncoder::getSpeed(void)
{
    return mCurSpeed;
}

void StepperEncoder::initTimer(void)
{
    u8 div;

    TCCR1A = 0;
    TCCR1B = 0;

    switch (T1_DIV) {
        case 8:
            div = _BV(CS11);
            break;

        case 64:
            div = _BV(CS11) | _BV(CS10);
            break;

        case 256:
            div = _BV(CS12);
            break;

        case 1024:
        default:
            div = _BV(CS12) | _BV(CS10);
            break;
    }

    TCCR1B |= div;
    TCNT1   = 0;
}


void StepperEncoder::setup(u8 mode)
{
    u8 data[2];

    mI2C.read(ENCODERADDR, ANGLE, 2, data);
    mEncoderOffset = MAKE_WORD(data);
    LOG(F("Offset : %d\n"), mEncoderOffset);

    mPeriod = T1_FREQ / 1000;
    mOldAngle = 0;
    mAngleMoved = 0;
    mRevolutions = 0;
//    initTimer();
//    nextISR();
//    enableInt(TRUE);
}

void StepperEncoder::setHome(void)
{
    u8 data[2];

    mI2C.read(ENCODERADDR, ANGLE, 2, data);
    mEncoderOffset = MAKE_WORD(data);
    mAngle = 0;
    mOldAngle = 0;
    mAngleMoved = 0;
    mRevolutions = 0;
}

float StepperEncoder::getAngle(void)
{
    return (float)mAngle * 0.087890625;
}

u16 StepperEncoder::getStrength(void)
{
    u8 data[2];

    mI2C.read(ENCODERADDR, MAGNITUDE, 2, data);
    return MAKE_WORD(data);
}

u8 StepperEncoder::getAgc(void)
{
    u8 data;

    mI2C.read(ENCODERADDR, AGC, 1, &data);

    return data;
}

u8 StepperEncoder::detectMagnet(void)
{
    u8 data;

    mI2C.read(ENCODERADDR, STATUS, 1, &data);

    data &= 0x38;                   //For some reason the encoder returns random values on reserved bits. Therefore we make sure reserved bits are cleared before checking the reply !

    if (data & 0x20) {              // magnet detected
        if (data & 0x10) {          // too weak
            data = 2;
        } else if (data & 0x08) {   // too strong
            data = 3;
        } else {                    // O.K.
            data = 1;
        }
    } else {                        // magnet is not detected
        data = 0;
    }

    return data;
}

void StepperEncoder::calcAngle(void)
{
    u8      data[2];
    u16     curAngle;
    s16     deltaAngle;
    float   newSpeed;
    static float deltaSpeedAngle = 0.0;
    static u8   loops = 0;

    if (!mI2C.read(ENCODERADDR, ANGLE, 2, data)) {
        LOG(F("ERROR !!!\n"));
    }
    curAngle = MAKE_WORD(data);
    mAngle    = curAngle;
    curAngle -= mEncoderOffset;
    if(curAngle > 4095) {
        curAngle -= 61440;
    }

    deltaAngle = (s16)mOldAngle - (s16)curAngle;
    if(deltaAngle < -2047) {
        mRevolutions--;
        deltaAngle += 4096;
    } else if(deltaAngle > 2047) {
        mRevolutions++;
        deltaAngle -= 4096;
    }

    if (loops < 10) {
        loops++;
        deltaSpeedAngle += (float)deltaAngle;
    } else {
        newSpeed = deltaSpeedAngle * ENCODERSPEEDCONSTANT;
        mCurSpeed = newSpeed;
        loops = 0;
        deltaSpeedAngle = 0.0;
    }
    mAngleMoved = (s32)curAngle + (4096 * (s32)mRevolutions);
    mOldAngle = curAngle;
}

static u32 mLast = 0;

void StepperEncoder::loop(u32 ts)
{
    static s16 oldAngleMoved = 0;
    s16 angleMoved;

    if (ts - mLast > 500) {
        u8 mag = detectMagnet();
        calcAngle();
        angleMoved = (s16)getAngleMoved();
        if (oldAngleMoved != angleMoved) {
            LOG(F("magnet:%d, angle:%d, moved:%d, revol:%d\n"), mag, (s16)getAngle(), angleMoved, mRevolutions);
            oldAngleMoved = angleMoved;
        }
        mLast = ts;
    }
}

