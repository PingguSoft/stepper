#include "ik.h"

//
// https://appliedgo.net/roboticarm/
//

#define	DEC_EXP_1		10
#define	DEC_EXP_2		100
#define	DEC_EXP_4		10000
#define	DEC_EXP_6		1000000
#define ACOS_RES_EXP5   616 // ((314159 / 2 + (255 / 2)) / 255)

//--------------------------------------------------------------------
//
// ArcCosinus Table
// Table build in to 3 part to get higher accuracy near cos = 1.
// The biggest error is near cos = 1 and has a biggest value of 3*0.012098rad = 0.521 deg.
// -    Cos 0 to 0.9 is done by steps of 0.0079 rad.   [1    / 127]
// -    Cos 0.9 to 0.99 is done by steps of 0.0008 rad [0.1  / 127]
// -    Cos 0.99 to 1 is done by step of 0.0002 rad    [0.01 /  64]
// Since the tables are overlapping the full range of 127+127+64 is not necessary. Total bytes: 277

static const u8 TBL_ACOS[] PROGMEM = {
    255, 254, 252, 251, 250, 249, 247, 246, 245, 243, 242, 241, 240, 238, 237, 236,
    234, 233, 232, 231, 229, 228, 227, 225, 224, 223, 221, 220, 219, 217, 216, 215,
    214, 212, 211, 210, 208, 207, 206, 204, 203, 201, 200, 199, 197, 196, 195, 193,
    192, 190, 189, 188, 186, 185, 183, 182, 181, 179, 178, 176, 175, 173, 172, 170,
    169, 167, 166, 164, 163, 161, 160, 158, 157, 155, 154, 152, 150, 149, 147, 146,
    144, 142, 141, 139, 137, 135, 134, 132, 130, 128, 127, 125, 123, 121, 119, 117,
    115, 113, 111, 109, 107, 105, 103, 101,  98,  96,  94,  92,  89,  87,  84,  81,
     79,  76,  73,  73,  73,  72,  72,  72,  71,  71,  71,  70,  70,  70,  70,  69,
     69,  69,  68,  68,  68,  67,  67,  67,  66,  66,  66,  65,  65,  65,  64,  64,
     64,  63,  63,  63,  62,  62,  62,  61,  61,  61,  60,  60,  59,  59,  59,  58,
     58,  58,  57,  57,  57,  56,  56,  55,  55,  55,  54,  54,  53,  53,  53,  52,
     52,  51,  51,  51,  50,  50,  49,  49,  48,  48,  47,  47,  47,  46,  46,  45,
     45,  44,  44,  43,  43,  42,  42,  41,  41,  40,  40,  39,  39,  38,  37,  37,
     36,  36,  35,  34,  34,  33,  33,  32,  31,  31,  30,  29,  28,  28,  27,  26,
     25,  24,  23,  23,  23,  23,  22,  22,  22,  22,  21,  21,  21,  21,  20,  20,
     20,  19,  19,  19,  19,  18,  18,  18,  17,  17,  17,  17,  16,  16,  16,  15,
     15,  15,  14,  14,  13,  13,  13,  12,  12,  11,  11,  10,  10,   9,   9,   8,
     7,   6,   6,   5,   3,   0
};



IK::IK(u16 l1, u16 l2)
{
    mL1 = l1;
    mL2 = l2;
}

s16 IK::acos(s16 cos4)
{
    bool fNegVal;
    s16  angleRad4 = 0;

    //Check for negative value
    if (cos4 < 0) {
        cos4 = -cos4;
        fNegVal = TRUE;
    } else
        fNegVal = FALSE;

    //Limit cos4 to his maximal value
    cos4 = min(cos4, DEC_EXP_4);

    if ((cos4 >= 0) && (cos4 < 9000)) {
        angleRad4 = pgm_read_byte(&TBL_ACOS[cos4 / 79]);
    } else if ((cos4 >= 9000) && (cos4 < 9900)) {
        angleRad4 = pgm_read_byte(&TBL_ACOS[(cos4 - 9000) / 8 + 114]);
    } else if ((cos4 >= 9900) && (cos4 <= 10000)) {
        angleRad4 = pgm_read_byte(&TBL_ACOS[(cos4 - 9900) / 2 + 227]);
    }
    angleRad4 = (s32)((s32)angleRad4 * ACOS_RES_EXP5) / DEC_EXP_1;

    //Add negative sign
    if (fNegVal)
        angleRad4 = 31416 - angleRad4;

    return angleRad4;

}

u32 IK::sqrt(u32 x)
{
    u32 xr;                     // result register
    u32 q2;                     // scan-bit register
    u8  f;                      // flag (one bit)

    xr = 0;                     // clear result
    q2 = 0x40000000L;           // higest possible result bit

    do
    {
        if ((xr + q2) <= x) {
            x -= xr + q2;
            f = 1;              // set flag
        } else {
            f = 0;              // clear flag
        }
        xr >>= 1;
        if (f) {
            xr += q2;           // test flag
        }
    } while(q2 >>= 2);          // shift twice

    if (xr < x) {
        return xr +1;           // add for rounding
    }
    return xr;
}

s16 IK::atan2(s16 y, s16 x)
{
    s16   angleRad4;
    s16   atan4;
    s32   temp;

    temp = sqrt(((s32)x * x * DEC_EXP_4) + ((s32)y * y * DEC_EXP_4));
    if (temp == 0)
        return 0;

    angleRad4 = acos(((s32)x * (s32)DEC_EXP_6) / temp);

    if (y < 0)
        atan4 = -angleRad4;
    else
        atan4 = angleRad4;

    return atan4;
}

s16 IK::lawOfCos(s32 a4, s32 b4, s32 c4)
{
    if (a4 == 0 || b4 == 0)
        return 0;

    s32 val = (a4 * a4 + b4 * b4 - c4 * c4) * DEC_EXP_4 / (2 * a4 * b4);
    return acos(val);
}

s32 IK::distance(s16 x, s16 y)
{
    return sqrt((s32)x * x * DEC_EXP_4 + (s32)y * y * DEC_EXP_4) * DEC_EXP_2;
}

void IK::getAngles(s16 x, s16 y, s16 *arad4, s16 *brad4)
{
    u32 dist = distance(x, y);
    s16 a1   = atan2(y, x);
    s16 a2   = lawOfCos(dist, (u32)mL1 * DEC_EXP_4, (u32)mL2 * DEC_EXP_4);

    *arad4 = a1 + a2;
    *brad4 = lawOfCos((u32)mL1 * DEC_EXP_4, (u32)mL2 * DEC_EXP_4, dist);
}

s16 IK::rad2deg(s16 rad4)
{
    return ((s32)rad4 * 180) / 31415;
}

