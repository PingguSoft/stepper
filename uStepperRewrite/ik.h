/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef _IK_H_
#define _IK_H_
#include <Arduino.h>
#include "common.h"
#include "config.h"
#include "utils.h"

class IK
{
    public:
        IK(u16 l1, u16 l2);
        u32 sqrt(u32 x);                                        // return exp0
        s32 distance(s16 x, s16 y);                             // return exp4
        s16 rad2deg(s16 rad4);                                  // return exp0
        void getAngles(s16 x, s16 y, s16 *arad4, s16 *brad4);   //
        void getAngles(s16 x, s16 y, s16 z, s16 *arad4, s16 *brad4, s16 *crad4);

    private:
        u16 mL1;
        u16 mL2;

        s16 acos(s16 cos4);                                     // return exp4
        s16 atan2(s16 y, s16 x);                                // return exp4
        s16 lawOfCos(s32 a4, s32 b4, s32 c4);                   // return exp4
};
#endif
