#include <Arduino.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <avr/io.h>
#include "I2CMaster.h"
#include "utils.h"


/** I2C bus is not currently in use */
#define I2CFREE     0

/** Value for RW bit in address field, to request a read */
#define READ        1

/** Value for RW bit in address field, to request a write */
#define WRITE       0

/** start condition transmitted */
#define START       0x08

/** repeated start condition transmitted */
#define REPSTART    0x10

/** slave address plus write bit transmitted, ACK received */
#define TXADDRACK   0x18

/** data transmitted, ACK received */
#define TXDATAACK   0x28

/** slave address plus read bit transmitted, ACK received */
#define RXADDRACK   0x40

/** value to indicate ACK for i2c transmission */
#define ACK         1

/** value to indicate NACK for i2c transmission */
#define NACK        0


bool I2CMaster::cmd(u8 cmd)
{
    u16 i = 0;
    // send command
    TWCR = cmd;

    // wait for command to complete
    while (!(TWCR & _BV(TWINT))) {
        i++;
        if (i == 65000) {
            return false;
        }
    }

    // save status bits
    status = TWSR & 0xF8;

    return true;
}

bool I2CMaster::read(u8 slaveAddr, u8 regAddr, u8 numOfBytes, u8 *data)
{
    u8 i, buff[numOfBytes];

    if (mEnableInt)
        (*mEnableInt)(false);

    if (start(slaveAddr, WRITE) == false) {
        stop();
        return false;
    }

    if(writeByte(regAddr) == false) {
        stop();
        return false;
    }

    if(restart(slaveAddr, READ) == false) {
        stop();
        return false;
    }

    for(i = 0; i < (numOfBytes - 1); i++) {
        if(readByte(ACK, &data[i]) == false) {
            stop();
            return false;
        }
    }

    if(readByte(NACK, &data[numOfBytes-1]) == false) {
        stop();
        return false;
    }

    stop();

    if (mEnableInt)
        (*mEnableInt)(true);

    return 1;
}

bool I2CMaster::write(u8 slaveAddr, u8 regAddr, u8 numOfBytes, u8 *data)
{
    u8 i;

    if (mEnableInt)
        (*mEnableInt)(false);

    if (start(slaveAddr, WRITE) == false) {
        stop();
        return false;
    }

    if (writeByte(regAddr) == false) {
        stop();
        return false;
    }

    for (i = 0; i < numOfBytes; i++) {
        if(writeByte(*(data + i)) == false) {
            stop();
            return false;
        }
    }
    stop();

    if (mEnableInt)
        (*mEnableInt)(true);

    return 1;
}

bool I2CMaster::readByte(bool ack, u8 *data)
{
    if (ack) {
        if (cmd(_BV(TWINT) | _BV(TWEN) | _BV(TWEA)) == false) {
            return false;
        }
    } else {
        if (cmd(_BV(TWINT) | _BV(TWEN)) == false) {
            return false;
        }
    }

    *data = TWDR;

    return true;
}

bool I2CMaster::start(u8 addr, bool RW)
{
    // send START condition
    cmd(_BV(TWINT) | _BV(TWSTA) | _BV(TWEN));

    if (getStatus() != START && getStatus() != REPSTART) {
        return false;
    }

    // send device address and direction
    TWDR = (addr << 1) | RW;
    cmd(_BV(TWINT) | _BV(TWEN));

    if (RW == READ) {
        return getStatus() == RXADDRACK;
    } else {
        return getStatus() == TXADDRACK;
    }
}

bool I2CMaster::restart(u8 addr, bool RW)
{
    return start(addr, RW);
}

bool I2CMaster::writeByte(u8 data)
{
    TWDR = data;

    cmd(_BV(TWINT) | _BV(TWEN));

    return getStatus() == TXDATAACK;
}

bool I2CMaster::stop(void)
{
    u16 i = 0;
    //  issue stop condition
    TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);

    // wait until stop condition is executed and bus released
    while (TWCR & _BV(TWSTO));

    status = I2CFREE;

    return 1;
}

u8 I2CMaster::getStatus(void)
{
    return status;
}

void I2CMaster::begin(u32 freq = 100000L)
{
    // activate internal pullups for twi.
    digitalWrite(SDA, HIGH);
    digitalWrite(SCL, HIGH);

    // initialize twi prescaler and bit rate
    TWSR &= ~(_BV(TWPS0) | _BV(TWPS1));
    TWBR = ((F_CPU / freq) - 16) / 2;

    /* twi bit rate formula from atmega128 manual pg 204
    SCL Frequency = CPU Clock Frequency / (16 + (2 * TWBR))
    note: TWBR should be 10 or higher for master mode
    It is 72 for a 16mhz Wiring board with 100kHz TWI */
}

I2CMaster::I2CMaster(void)
{

}

