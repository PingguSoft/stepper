#include <Arduino.h>
#include <stdarg.h>
#include <Wire.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "Stepper.h"
#include "RobotArm.h"
#include "StepperEncoder.h"

/*
*****************************************************************************************
* VARIABLES
*****************************************************************************************
*/
static Stepper mStepper;


/*
*****************************************************************************************
* ATCmd class
*****************************************************************************************
*/
class ATCmd
{
public:
    ATCmd();
    ~ATCmd();

    u8   loop(void);
    void registerCallback(void *param, s8 (*callback)(void *param, u8 *data, u8 size, u8 *res));
    void printf(char *fmt, ...);
    void printf(const __FlashStringHelper *fmt, ...);

private:
    typedef enum
    {
        STATE_IDLE,
        STATE_HEADER_START,
        STATE_HEADER_T,
        STATE_HEADER_BODY,
    } STATE_T;
    //

    // variables
    u8   mRxPacket[32];

    u8   mState;
    u8   mOffset;
    s8  (*mCallback)(void *param, u8 *data, u8 size, u8 *res);
    void *mParam;

    void evalATCommand(u8 *data, u8 size);
};

ATCmd::ATCmd()
{
    mState  = STATE_IDLE;
    mOffset = 0;
}

ATCmd::~ATCmd()
{
}

void ATCmd::registerCallback(void *param, s8 (*callback)(void *param, u8 *data, u8 size, u8 *res))
{
    mState = STATE_IDLE;
    mCallback = callback;
    mParam = param;
}

void ATCmd::evalATCommand(u8 *data, u8 size)
{
    u8  buf[8];
    u16 *rc;

    if (mCallback) {
        memset(&buf, 0, sizeof(buf));
        s8 ret = (*mCallback)(mParam, data, size, buf);
        if (ret >= 0) {
            Serial.print((char*)buf);
        }
    }
}

u8 ATCmd::loop(void)
{
    u8 ret = 0;
    u8 rxSize = Serial.available();

    if (rxSize == 0)
        return ret;

    while (rxSize--) {
        u8 ch = Serial.read();

        switch (mState) {
            case STATE_IDLE:
                if (ch == 'A' || ch == 'a')
                    mState = STATE_HEADER_START;
                break;

            case STATE_HEADER_START:
                mState = (ch == 'T' || ch == 't') ? STATE_HEADER_T : STATE_IDLE;
                break;

            case STATE_HEADER_T:
                mRxPacket[mOffset++] = ch;
                if (mOffset >= 32) {
                    mState = STATE_IDLE;
                    mOffset = 0;
                } else {
                    if (ch == '\n' || ch == '\r') {
                        evalATCommand(mRxPacket, mOffset - 1);
                        mState = STATE_IDLE;
                        mOffset = 0;
                    }
                }
                break;
        }
    }
    return ret;
}

void ATCmd::printf(char *fmt, ...)
{
    char buf[32];
    va_list args;

    va_start (args, fmt);
    vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);
    Serial.write(buf);
}

void ATCmd::printf(const __FlashStringHelper *fmt, ...)
{
    char buf[32];
    va_list args;

    va_start (args, fmt);
    vsnprintf_P(buf, sizeof(buf), (const char *)fmt, args); // progmem for AVR
    va_end(args);

    Serial.write(buf);
}


static ATCmd mAT;

s16 mStep = 50;
u16 mAccel = 50;
u16 mDecel = 50;
u16 mSpeed = 50;
u8  mMot   = 0;
bool mEnable = FALSE;

u32          mLastTS = 0;
RobotArm     robot(SHOULDER_TO_ELBOW, ELBOW_TO_WRIST, WRIST_TO_FINGER);
StepperEncoder  mEnc;

s8 ATCallback(void *param, u8 *data, u8 size, u8 *res)
{
    s8 ret = 4;
    s32 deg = 0;

    if (size == 0) {
        LOG(F("step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), mStep, mAccel, mDecel, mSpeed);
        mStepper.move(mMot, mStep, mAccel, mDecel, mSpeed);
    } else {
        switch(*data) {

#if 1
            case 'i':
                mAT.printf(F("ADDR=%02x\n"), mStepper.getAddr());
                break;

            case 'r':
            {
                u8 addr = strtol(data + 1, NULL, 16);
                mAT.printf(F("Set ADDR=%02x\n"), addr);
                mStepper.setAddr(addr);
                break;
            }

            case 'q':
                mAT.printf(F("Save\n"));
                mStepper.save();
                break;
#endif
            case 'x':
                mMot = 0;
                LOG(F("motor_x\n"));
                break;
            case 'y':
                mMot = 1;
                LOG(F("motor_y\n"));
                break;
            case 'z':
                mMot = 2;
                LOG(F("motor_z\n"));
                break;

            case 'p':
                mEnable = !mEnable;
                LOG(F("motor enable : %d\n"), mEnable);
                Stepper::enable(mEnable);
                if (mEnable) {
                    mEnc.setHome();
                }
                break;

            case 'a':
                mAccel = strtol(data + 1, NULL, 10);
                LOG(F("step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), mStep, mAccel, mDecel, mSpeed);
                break;

            case 'd':
                mDecel = strtol(data + 1, NULL, 10);
                LOG(F("step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), mStep, mAccel, mDecel, mSpeed);
                break;

            case 's':
                mSpeed = strtol(data + 1, NULL, 10);
                LOG(F("step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), mStep, mAccel, mDecel, mSpeed);
                break;

            case 'u':
                deg = strtol(data + 1, NULL, 10);
                mStep =  MOTOR_FSPR * GEAR_RATIO_X * DEG2RAD(deg) / (2 * PI);
                mMot = 1;
                LOG(F("step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), mStep, mAccel, mDecel, mSpeed);
                mStepper.move(mMot, mStep, mAccel, mDecel, mSpeed);
                break;

            case 'm':
                {
                u8 tbl[3] = {'x', 'y', 'z'};
                mStep = strtol(data + 1, NULL, 10);
                LOG(F("mot:%c, step:%5d, accel:%5d, decel:%5d, speed:%5d\n"), tbl[mMot], mStep, mAccel, mDecel, mSpeed);
                mStepper.move(mMot, mStep, mAccel, mDecel, mSpeed);
                }
                break;

            case 't':
                for (int i = 0; i < 200; i++) {
                    mStepper.step(mMot, CW);
                    delayMicroseconds(2000);
                }
                break;

            default:
                ret = -1;
                break;
        }
    }

    if (ret >= 0) {
        strcpy_P(res, PSTR("OK\n"));
    } else {
        strcpy_P(res, PSTR("ERR\n"));
        ret = 5;
    }

    return ret;
}

/*
*****************************************************************************************
* setup
*****************************************************************************************
*/

void setup()
{
#if __STD_SERIAL__
    Serial.begin(57600);
#endif

    while (!Serial);

    mStepper.setup();
    mAT.registerCallback((void*)NULL, ATCallback);
    LOG(F("Let's start...\n"));

    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, HIGH);

    robot.setup(&Serial);
    mEnc.setup(0);
    mLastTS = millis();
}


void toggleLED(void)
{
    static u8 led = 1;

    led = !led;
    digitalWrite(PIN_LED, led);
}

void loop()
{
    u32 ts = millis();

    robot.loop(ts);
    mEnc.loop(ts);

//    mStepper.loop(ts);
//    mAT.loop();

    if (ts - mLastTS > 500) {
        toggleLED();
        mLastTS = ts;
    }
}

