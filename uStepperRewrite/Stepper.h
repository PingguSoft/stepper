/*
 This project is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 see <http://www.gnu.org/licenses/>
*/


#ifndef _STEPPER_H_
#define _STEPPER_H_
#include <stdarg.h>
#include "common.h"
#include "config.h"
#include "utils.h"
#include "SerialProtocol.h"
#include "I2CMaster.h"

#define CW      0
#define CCW     1

extern "C" void TIMER3_COMPA_vect(void) __attribute__ ((signal,used));
extern "C" void TIMER3_COMPB_vect(void) __attribute__ ((signal,used));
extern "C" void TIMER3_COMPC_vect(void) __attribute__ ((signal,used));

class Stepper
{
public:
    enum {
        // Speed ramp states
        STOP  = 0,
        ACCEL = 1,
        DECEL = 2,
        RUN   = 3
    };

    Stepper();
    ~Stepper();
    void loop(u32 ts);
    u8   getAddr(void)      { return mInfo.addr; }
    void setAddr(u8 addr)   { mInfo.addr = addr; }
    void save(void);
    void move(u8 idx, s16 step, u16 accel, u16 decel, u16 speed);
    static void step(u8 idx, u8 dir);
    static void enable(bool en);
    void setup(void);

private:
    struct info {
        u8  addr;
    };

    struct rampData {
      u8    run_state;          //! What part of the speed ramp we are in.
      u8    dir;                //! Direction stepper motor should move.
      u16   step_delay;         //! Peroid of next timer delay. At start this value set the accelration rate.
      u16   decel_start;        //! What step_pos to start decelaration
      s16   decel_val;          //! Sets deceleration rate.
      u16   min_delay;          //! Minimum time delay (max speed)
      s16   accel_count;        //! Counter used when accelerateing/decelerateing to calculate step_delay.

      s16   last_accel_delay;   // Remember the last step delay used when accelrating.
      u16   step_count;         // Counting steps when moving.
      s16   rest;               // Keep track of remainder from new_step-delay calculation to incrase accurancy
    } ;


    struct rampData mSRD[DOF];
    struct info     mInfo;
    SerialProtocol  mSerial;
    u32             mTS;
    u16             mCtr;

    void enableTimer(u8 idx, bool enable);
    void nextISR(u8 idx, u16 delay);
    void doISR(u8 idx);
    void initTimer(void);
    u32  sqrtl(u32 x);

    friend s8 mspCallback(u8 addr, u8 *data, u8 size, u8 *res);
    friend void TIMER3_COMPA_vect(void) __attribute__ ((signal,used));
    friend void TIMER3_COMPB_vect(void) __attribute__ ((signal,used));
    friend void TIMER3_COMPC_vect(void) __attribute__ ((signal,used));
};

#endif

