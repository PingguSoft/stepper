#ifndef _STEPPER_ENCODER_H_
#define _STEPPER_ENCODER_H_
#include "common.h"
#include "config.h"
#include "utils.h"
#include "I2CMaster.h"

extern "C" void TIMER1_COMPA_vect(void) __attribute__ ((signal,used));


class StepperEncoder
{
public:
    /**
     * @brief      Constructor
     *
     *             This is the constructor of the uStepperEncoder class.
     */
    StepperEncoder(void);

    /**
     * @brief      Measure the current shaft angle
     *
     *             This function reads the current angle of the motor shaft. The
     *             resolution of the angle returned by this function is
     *             0.087890625 degrees (12 bits) The Angle is read by means of
     *             the I2C interface, using the I2C interface implemented in
     *             this library.
     *
     * @return     Floating point representation of the current motor shaft
     *             angle
     */
    float getAngle(void);

    /**
     * @brief      Measure the current speed of the motor
     *
     *             This function returns the current speed of the motor. The
     *             speed is not calculated in this function, it is merely
     *             returning a variable. The speed is calculated in the
     *             interrupt routine associated with timer1.
     *
     * @return     Current speed in revolutions per second (RPS)
     */
    float getSpeed(void);

    /**
     * @brief      Measure the strength of the magnet
     *
     *             This function returns the strength of the magnet
     *
     * @return     Strength of magnet
     */
    u16 getStrength(void);

    /**
     * @brief      Read the current AGC value of the encoder chip
     *
     *             This function returns the current value of the AGC register
     *             in the encoder chip (AS5600). This value ranges between 0 and
     *             255, and should preferably be as close to 128 as possible.
     *
     * @return     current AGC value
     */
    u8 getAgc(void);

    /**
     * @brief      Detect if magnet is present and within range
     *
     *             This function detects whether the magnet is present, too
     *             strong or too weak.
     *
     * @return     0 - Magnet detected and within limits
     * @return     1 - Magnet too strong
     * @return     2 - Magnet too weak
     */
    u8 detectMagnet(void);

    /**
     * @brief      Measure the angle moved from reference position
     *
     *             This function measures the angle moved from the shaft
     *             reference position. When the uStepper is first powered on,
     *             the reference position is reset to the current shaft
     *             position, meaning that this function will return the angle
     *             rotated with respect to the angle the motor initially had. It
     *             should be noted that this function is absolute to an
     *             arbitrary number of revolutions !
     *
     *             The reference position can be reset at any point in time, by
     *             use of the setHome() function.
     *
     * @return     The angle moved.
     */
    float getAngleMoved(void);

    /**
     * @brief      Setup the encoder
     *
     *             This function initializes all the encoder features.
     *
     * @param[in]  mode  Variable to indicate if the uStepper is in normal or
     *                   drop-in mode
     */
    void setup(u8 mode);

    /**
     * @brief      Define new reference(home) position
     *
     *             This function redefines the reference position to the current
     *             angle of the shaft
     */
    void setHome(void);

    void loop(u32 ts);
    void calcAngle(void);

private:

    /** Variable used to store that measured angle moved from the
    * reference position */
    s32 mAngleMoved;

    /** Angle of the shaft at the reference position. */
    u16 mEncoderOffset;

    /** Used to stored the previous measured angle for the speed
     * measurement, and the calculation of angle moved from reference
     * position */
    u16 mOldAngle;

    /** This variable always contain the current rotor angle, relative
    * to a single revolution */
    u16 mAngle;

    /** This variable contains the number of revolutions in either
     * direction, since last home position was set. negative numbers
     * corresponds to CCW turns, and positive to CW turns */
    s16 mRevolutions;

    /** Variable used to store the last measured rotational speed of
    * the motor shaft */
    float mCurSpeed;

    I2CMaster       mI2C;
    u16             mPeriod;

    void        initTimer(void);
    inline void nextISR(void)   { OCR1A = TCNT1 + mPeriod; }

    friend void TIMER1_COMPA_vect(void) __attribute__ ((signal,used));
    friend void enableInt(bool enable);
};

#endif
